let varNum = prompt("Input a number:");

for (let i = varNum; i >= 0; i--) {
	if (i <= 50) {
		console.log("The current value is at " + i + ". Terminating the loop.")
		break;
	}
	else if (i % 10 == 0) {
		console.log("The number you have provided is divisible by 10. Skipping this number.");
		continue;
	}
	else if (i % 5 == 0) {
		console.log(i);
	}
}

let newString = "supercalifragilisticexpialidocious";
let stringConsonant = "";

for (i = 0; i < newString.length; i++) {
	if (newString[i] == "a" ||
		newString[i] == "e" ||
		newString[i] == "i" ||
		newString[i] == "o" ||
		newString[i] == "u") {
		continue;
	}
	else {
		stringConsonant = stringConsonant + newString[i];
	}
}

console.log(newString);
console.log(stringConsonant);